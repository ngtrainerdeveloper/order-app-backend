# Order-App Microservices Architecture

## Overview
Welcome to the Order-App repository! This project demonstrates a microservices architecture built using Spring Boot and Kafka for event-driven development. The system comprises several microservices, each responsible for specific business functionalities related to order management. The services include Order, Customer, Product, Notification, Payment, Gateway, and Config Server.

## Microservices

### 1. Order Service
**Description:** Manages customer orders, including creation, updating, and retrieval of order details.  
**Endpoints:**  
- `POST /orders` - Create a new order
- `GET /orders/{id}` - Retrieve order details by ID
- `PUT /orders/{id}` - Update order details
- `DELETE /orders/{id}` - Delete an order

### 2. Customer Service
**Description:** Handles customer-related operations such as registration, authentication, and profile management.  
**Endpoints:**  
- `POST /customers` - Register a new customer
- `GET /customers/{id}` - Retrieve customer details by ID
- `PUT /customers/{id}` - Update customer information
- `DELETE /customers/{id}` - Delete a customer

### 3. Product Service
**Description:** Manages product catalog including product details, availability, and pricing.  
**Endpoints:**  
- `POST /products` - Add a new product
- `GET /products/{id}` - Retrieve product details by ID
- `PUT /products/{id}` - Update product information
- `DELETE /products/{id}` - Delete a product

### 4. Notification Service
**Description:** Sends notifications to customers regarding their orders, updates, and promotions.  
**Endpoints:**  
- `POST /notifications` - Send a new notification
- `GET /notifications/{id}` - Retrieve notification details by ID

### 5. Payment Service
**Description:** Manages payment processing for customer orders.  
**Endpoints:**  
- `POST /payments` - Process a new payment
- `GET /payments/{id}` - Retrieve payment details by ID

### 6. Gateway Service
**Description:** Serves as an API Gateway, routing requests to appropriate services and handling cross-cutting concerns such as authentication and logging.  
**Endpoints:**  
- Routes requests to appropriate microservices

### 7. Config Server
**Description:** Centralized configuration management for all microservices, ensuring consistency and easier management of configuration properties.  
**Endpoints:**  
- Provides configuration properties to other microservices

## Event-Driven Development with Kafka

### Overview
The system leverages Kafka for event-driven communication between microservices, ensuring loose coupling and scalability. Each service produces and consumes events to perform business operations asynchronously.

### Kafka Topics
- `order-events`: Events related to order creation, updates, and deletion.
- `customer-events`: Events related to customer actions.
- `product-events`: Events related to product updates.
- `payment-events`: Events related to payment processing.
- `notification-events`: Events for sending notifications.

### Example Workflow
1. An order is created in the Order Service.
2. An event is published to the `order-events` topic.
3. The Payment Service consumes the event to process the payment.
4. Upon successful payment, the Notification Service consumes the event to send a notification to the customer.

## Getting Started

### Prerequisites
- JDK 11 or higher
- Apache Kafka
- Docker (optional, for containerization)
- Maven

### Running the Application
1. **Clone the Repository:**
    ```bash
    git clone https://gitlab.com/your-repo/order-app.git
    cd order-app
    ```
2. **Start Kafka:**
    Follow the instructions to start your Kafka server.

3. **Build and Run Microservices:**
    ```bash
    mvn clean install
    cd order-service
    mvn spring-boot:run
    # Repeat for each service
    ```

### Configuration
Each microservice retrieves its configuration from the Config Server. Update the `application.yml` in the Config Server repository to change the configurations.

## Contributing
Contributions are welcome! Please fork the repository and create a pull request with your changes. Ensure your code adheres to the project's coding standards and includes appropriate tests.

## License
This project is licensed under the RKD License.

## Contact
For any inquiries or support, please contact [ngtrainerdeveloper@gmail.com](mailto:ngtrainerdeveloper@gmail.com).
