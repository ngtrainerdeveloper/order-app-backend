package org.rdutta.gateway.controller;

import org.rdutta.gateway.model.AuthResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.annotation.RegisteredOAuth2AuthorizedClient;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/oauth")
public class AuthController {
    private final Logger log = LoggerFactory.getLogger(AuthController.class);

    /*
      Here we will get okta screen first then once login done login() method will work
    */
    @GetMapping("/login")
    public ResponseEntity<AuthResponse> login(@RegisteredOAuth2AuthorizedClient OAuth2AuthorizedClient client,
                                              @AuthenticationPrincipal OidcUser oidcUser,
                                              Model model){
        log.info("user email address: {}", oidcUser.getEmail());
        List<String> collectAuthorities = oidcUser.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());
        AuthResponse authResponse = AuthResponse.builder()
                .userId(oidcUser.getEmail())
                .accessToken(client.getAccessToken().getTokenValue())
                .refreshToken(Objects.requireNonNull(client.getRefreshToken()).getTokenValue())
                .expireAt(Objects.requireNonNull(client.getAccessToken().getExpiresAt()).getEpochSecond())
                .authorities(collectAuthorities)
                .build();

        return ResponseEntity.ok(authResponse);
    }
}
