package org.rdutta.notification.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
public class MailSenderConfig {
    @Bean
    public JavaMailSenderImpl mailSender() {
        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();

        javaMailSender.setHost("smtp.office365.com");
        javaMailSender.setPort(587);
        javaMailSender.setUsername("angdevlab@outlook.com");
        javaMailSender.setPassword("Prerna@12345");

        Properties props = javaMailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.connection-timeout", 5000);
        props.put("mail.smtp.timeout", 3000);
        props.put("mail.smtp.write-timeout", 5000);

        return javaMailSender;
    }
}
