package org.rdutta.notification.utils;

public enum NotificationType {
    ORDER_CONFIRMATION,
    PAYMENT_CONFIRMATION
}
