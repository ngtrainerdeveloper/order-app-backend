package org.rdutta.notification.utils.order;

import org.rdutta.notification.utils.payment.PaymentMethod;


import java.math.BigDecimal;
import java.util.List;

public record OrderConfirmation(
        String orderReference,
        BigDecimal totalAmount,
        PaymentMethod paymentMethod,
        Customer customer,
        List<Product> products

) {
}