package org.rdutta.notification.repository;

import org.rdutta.notification.entity.Notification;
import org.springframework.data.mongodb.repository.MongoRepository;



public interface NotificationRepository extends MongoRepository<Notification, String> {
}
