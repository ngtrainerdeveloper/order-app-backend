package org.rdutta.notification.utils.order;

import java.util.UUID;

public record Customer(
        UUID id,
        String firstname,
        String lastname,
        String email,
        String phone
) {

}
