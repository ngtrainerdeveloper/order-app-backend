package org.rdutta.payment.service;

import lombok.RequiredArgsConstructor;
import org.rdutta.payment.mapper.PaymentMapper;
import org.rdutta.payment.producers.NotificationProducer;
import org.rdutta.payment.record.PaymentNotificationRequest;
import org.rdutta.payment.record.PaymentRequest;
import org.rdutta.payment.repository.PaymentRepository;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PaymentService {
    private final PaymentRepository paymentRepository;
    private final PaymentMapper paymentMapper;
    private final NotificationProducer producer;
    public Integer createPayment(PaymentRequest request) {
        var payment = paymentRepository.save(paymentMapper.toPayment(request));

        producer.sendNotification(
                new PaymentNotificationRequest(
                        request.orderReference(),
                        request.amount(),
                        request.paymentMethod(),
                        request.customer().firstName(),
                        request.customer().lastName(),
                        request.customer().email()
                )
        );
        return payment.getId();
    }
}
