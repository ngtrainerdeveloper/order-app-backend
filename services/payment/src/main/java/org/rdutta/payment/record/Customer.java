package org.rdutta.payment.record;


import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import org.springframework.validation.annotation.Validated;

import java.util.UUID;

@Validated
public record Customer(
        UUID id,
        @NotNull(message = "FistName should not be null.")
        String firstName,
        @NotNull(message = "LastName should not be null.")
        String lastName,
        @NotNull(message = "Email is required.")
        @Email(message = "The customer is not correctly formatted.")
        String email,
        String phone
) {
}
