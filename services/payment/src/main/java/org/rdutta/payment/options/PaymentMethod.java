package org.rdutta.payment.options;

public enum PaymentMethod {
    PAYPAL,
    CREDIT_CARD,
    VISA,
    MASTER_CARD,
    BITCOIN,
    PHONEPE,
    GOOGLEPAY
}
