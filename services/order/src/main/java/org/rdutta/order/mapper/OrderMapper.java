package org.rdutta.order.mapper;

import org.rdutta.order.entity.Order;
import org.rdutta.order.record.OrderRequest;
import org.rdutta.order.record.OrderResponse;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderMapper {


    public Order toOrder(OrderRequest request) {
        return Order.builder()
                .id(request.id())
                .customerId(request.customerId())
                .reference(request.reference())
                .totalAmount(request.amount())
                .paymentMethod(request.paymentMethod())
                .build();
    }

    public OrderResponse fromOrder(Order order) {
        return new OrderResponse(
                order.getId(),
                order.getReference(),
                order.getTotalAmount(),
                order.getPaymentMethod(),
                order.getCustomerId()
        );
    }
}
