package org.rdutta.order.record;

import org.rdutta.order.options.PaymentMethod;

import java.math.BigDecimal;
import java.util.UUID;

public record OrderResponse(
        Integer id,
        String reference,
        BigDecimal amount,
        PaymentMethod paymentMethod,
        UUID customerId
) {
}
