package org.rdutta.order.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.rdutta.order.record.OrderRequest;
import org.rdutta.order.record.OrderResponse;
import org.rdutta.order.service.OrderService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/orders")
@RequiredArgsConstructor
public class OrderController {
    private final OrderService orderService;

    @PreAuthorize("hasAuthority('SCOPE_internal') || hasAuthority('Admin')")
    @PostMapping("/create")
    public ResponseEntity<Integer> createOrder(@RequestBody @Valid OrderRequest request) {
        return ResponseEntity.ok(this.orderService.createOrder(request));
    }

    @PreAuthorize("hasAuthority('SCOPE_internal') || hasAuthority('Admin')")
    @GetMapping("/allOrders")
    public ResponseEntity<List<OrderResponse>> getOrders() {
        return ResponseEntity.ok(this.orderService.findAllOrders());
    }

    @PreAuthorize("hasAuthority('SCOPE_internal') || hasAuthority('Admin')")
    @GetMapping("{order-id}")
    public ResponseEntity<OrderResponse> findById(@PathVariable("order-id") Integer orderId) {
        return ResponseEntity.ok(this.orderService.findOrderById(orderId));
    }
}
