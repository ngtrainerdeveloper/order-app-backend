package org.rdutta.order.record;

public record OrderLineResponse(
        Integer id,
        Double quantity
) {
}
