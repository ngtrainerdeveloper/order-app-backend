package org.rdutta.order.record;

import java.util.UUID;

public record CustomerResponse(
        UUID customerId,
        String firstName,
        String lastName,
        String email
) {
}
