package org.rdutta.order.record;

import com.fasterxml.jackson.annotation.JsonInclude;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.rdutta.order.options.PaymentMethod;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@JsonInclude(Include.NON_EMPTY)
public record OrderRequest(
        Integer id,
        String reference,
        @Positive(message = "Order amount should be positive.")
        BigDecimal amount,
        @NotNull(message = "Payment method should be precised.")
        PaymentMethod paymentMethod,
        @NotNull(message = "Customer should be present.")
        UUID customerId,
        @NotEmpty(message = "You should at least purchase one product.")
        List<PurchaseRequest> products
) {
}
