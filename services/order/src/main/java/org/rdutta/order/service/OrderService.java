package org.rdutta.order.service;


import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.rdutta.order.customer.CustomerClient;
import org.rdutta.order.customexception.BusinessException;
import org.rdutta.order.mapper.OrderMapper;
import org.rdutta.order.message.producer.OrderProducer;
import org.rdutta.order.payment.PaymentClient;
import org.rdutta.order.product.ProductClient;
import org.rdutta.order.record.*;
import org.rdutta.order.repository.OrderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderService {

    private final CustomerClient customerClient;
    private final ProductClient productClient;
    private final OrderRepository orderRepository;
    private final OrderMapper orderMapper;
    private final OrderLineService orderLineService;
    private final OrderProducer orderProducer;
    private final PaymentClient client;
    @Transactional
    public Integer createOrder(OrderRequest request) {
        //TODO: Check the customer
        var customer = this.customerClient.findCustomerById(request.customerId())
                .orElseThrow(() -> new BusinessException("Can't create order:: No Customer found with customer id: " + request.customerId()));

        //TODO: Purchase the products --> product-microservice(We use RestTemplate)
       var purchasedProducts =  productClient.purchaseProducts(request.products());

        //TODO: Persist Order
        var order = this.orderRepository.save(orderMapper.toOrder(request));

        //TODO: Persist order lines
        for (PurchaseRequest purchaseRequest : request.products()) {
            orderLineService.saveOrderLine(
                    new OrderLineRequest(
                            null,
                            order.getId(),
                            purchaseRequest.productId(),
                            purchaseRequest.quantity()
                    )
            );
        }
        //TODO: start payment process
        var paymentRequest = new PaymentRequest(
                request.amount(),
                request.paymentMethod(),
                request.id(),
                request.reference(),
                customer
        );
        client.requestOrderPayment(paymentRequest);
        //TODO: send the order confirmation --> notification-microservice (here will use Kafka)
        orderProducer.sendOrderConfirmation(
                new OrderConfirmation(
                        request.reference(),
                        request.amount(),
                        request.paymentMethod(),
                        customer,
                        purchasedProducts
                )
        );
        return order.getId();
    }

    public List<OrderResponse> findAllOrders() {
        return orderRepository.findAll().stream().map(orderMapper::fromOrder)
                .collect(Collectors.toList());
    }

    public OrderResponse findOrderById(Integer orderId) {
        return orderRepository.findById(orderId).map(orderMapper::fromOrder)
                .orElseThrow(() -> new EntityNotFoundException(String.format("No Order found with the provided Id:: %d",orderId)));
    }
}
