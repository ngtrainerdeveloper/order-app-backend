package org.rdutta.customer;

import com.google.gson.Gson;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.hamcrest.Matchers;
import org.rdutta.customer.entity.Address;
import org.rdutta.customer.record.CustomerRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;
import java.util.UUID;


public class CustomerAPITest {

    private static final String BASE_URI = "http://localhost:8050/api/v1/customer";
    private static final Logger log = LoggerFactory.getLogger(CustomerAPITest.class);
    private static UUID testCustomerId;
    private static UUID testLocalId;
    @BeforeClass
    public void setup() {

        RestAssured.baseURI = BASE_URI;

        // Stub the /create endpoint
        testCustomerId = UUID.randomUUID();
        testLocalId = UUID.randomUUID();
    }

    @AfterClass
    public void teardown() {
    }

    @Test
    public void createCustomer() {
        // Construct CustomerRequest
        Address address = Address.builder()
                .localId(testLocalId)
                .street("20th H Cross Road")
                .city("Bengaluru")
                .state("Karnataka")
                .zip("560047")
                .country("India")
                .build();

        CustomerRequest customerRequest = new CustomerRequest(
                testCustomerId,
                "Raj",
                "Dutta",
                "rdutta@orderapp.com",
                "7992204910",
                address
        );

        // Convert CustomerRequest to JSON
        Gson gson = new Gson();
        String jsonBody = gson.toJson(customerRequest);

        // Send POST request and validate response
        RestAssured
                .given()
                .contentType(ContentType.JSON)
                .body(jsonBody)
                .when()
                .post("/create")
                .then()
                .assertThat()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .body(Matchers.equalTo(testCustomerId));

        log.info("Successfully created Customer with id " + testCustomerId);
    }


    @Test
    public void testGetCustomerDetailsWithID() {
        // Send GET request to retrieve customer details by ID
        RestAssured
                .given()
                .contentType(ContentType.JSON)
                .when()
                .get("/" + testCustomerId)
                .then()
                .assertThat()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .body(
                        "customerId", Matchers.equalTo(testCustomerId),
                        "firstName", Matchers.equalTo("Raj"),
                        "lastName", Matchers.equalTo("Dutta"),
                        "email", Matchers.equalTo("rdutta@orderapp.com"),
                        "phone", Matchers.equalTo("7992204910"),
                        "address.localId", Matchers.equalTo(testLocalId),
                        "address.street", Matchers.equalTo("20th H Cross Road"),
                        "address.city", Matchers.equalTo("Bengaluru"),
                        "address.state", Matchers.equalTo("Karnataka"),
                        "address.zip", Matchers.equalTo("560047"),
                        "address.country", Matchers.equalTo("India")
                );


        log.info("Successfully retrieved Customer whose id relates: " + testCustomerId);
    }


    @Test
    public void testGetAllCustomerDetailsAtOnce() {
        RestAssured
                .given()
                .contentType(ContentType.TEXT)
                .when()
                .get("/findAll")
                .then()
                .assertThat()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .body("$",Matchers.instanceOf(List.class));

        log.info("Successfully retrieved all users, Present in the system.");
    }

    @Test(priority = 4)
    public void testUpdateCustomer() {
        // Construct CustomerRequest
        Address updateAddress = Address.builder()
                .localId(testLocalId)
                .street("20th H Cross Road")
                .city("Bengaluru")
                .state("Karnataka")
                .zip("560047")
                .country("India")
                .build();

        CustomerRequest updateCustomerRequest = new CustomerRequest(
                testCustomerId,
                "Ramesh",
                "Raj",
                "rraj@orderapp.com",
                "7992204910",
                updateAddress
        );

        // Convert CustomerRequest to JSON
        Gson gson = new Gson();
        String updateJsonBody = gson.toJson(updateCustomerRequest);

        Response response = RestAssured
                .given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(updateJsonBody)
                .when()
                .put("/update/"+testCustomerId);

        // Assert status code
        response.then().assertThat().statusCode(202);

        // Check if response has a body and Content-Type header
        if (response.getBody().asString().isEmpty()) {
            log.info("Successfully updated Customer whose id relates: " + testCustomerId);
        } else {
            response.then()
                    .contentType(ContentType.JSON)
                    .body(
                            "customerId", Matchers.equalTo(testCustomerId),
                            "firstName", Matchers.equalTo("Ramesh"),
                            "lastName", Matchers.equalTo("Raj"),
                            "email", Matchers.equalTo("rraj@orderapp.com"),
                            "phone", Matchers.equalTo("7992204910"),
                            "address.localId", Matchers.equalTo(testLocalId),
                            "address.street", Matchers.equalTo("20th H Cross Road"),
                            "address.city", Matchers.equalTo("Bengaluru"),
                            "address.state", Matchers.equalTo("Karnataka"),
                            "address.zip", Matchers.equalTo("560047"),
                            "address.country", Matchers.equalTo("India")
                    );
            log.info("Successfully updated Customer with details: " + response.getBody().asString());
        }
    }


    @Test(priority = 5)
    public void testDeleteCustomer() {
        RestAssured
                .given()
                .contentType(ContentType.TEXT)
                .when()
                .delete("/" + testCustomerId)
                .then()
                .assertThat()
                .statusCode(202);

        log.info("Successfully deleted Customer whose id relates: " + testCustomerId);
    }
}
