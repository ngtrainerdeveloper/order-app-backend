package org.rdutta.customer.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.rdutta.customer.record.CustomerRequest;
import org.rdutta.customer.record.CustomerResponse;
import org.rdutta.customer.service.CustomerService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/customers")
@RequiredArgsConstructor
public class CustomerController {
    private final CustomerService customerService;


    @PostMapping(value = "/create", produces = "application/json")
    public ResponseEntity<UUID> createCustomer(@RequestBody @Valid CustomerRequest customerRequest) {
        return ResponseEntity.ok(customerService.createCustomer(customerRequest));
    }


    @PreAuthorize("hasAuthority('Admin')")
    @PutMapping(value = "/update/{customerId}")
    public ResponseEntity<Void> updateCustomer(@RequestBody @Valid CustomerRequest customerRequest) {
        customerService.updateCustomer(customerRequest);
        return ResponseEntity.accepted().build();
    }

    @PreAuthorize("hasAuthority('SCOPE_internal') || hasAuthority('Admin')")
    @GetMapping("/findAll")
    public ResponseEntity<List<CustomerResponse>> findAll() {
        return ResponseEntity.ok(customerService.findAllCustomers());
    }


    @PreAuthorize("hasAuthority('SCOPE_internal') || hasAuthority('Administrator')")
    @GetMapping("/exists/{customerId}")
    public ResponseEntity<Boolean> exists(@PathVariable("customerId") UUID customerId) {
        return ResponseEntity.ok(customerService.existsById(customerId));
    }

    @PreAuthorize("hasAuthority('SCOPE_internal') || hasAuthority('Administrator')")
    @GetMapping("/{customerId}")
    public ResponseEntity<CustomerResponse> findByCustomerId(@PathVariable("customerId") UUID customerId) {
        return ResponseEntity.ok(customerService.findByCustomerId(customerId));
    }

    @PreAuthorize("hasAuthority('SCOPE_internal') || hasAuthority('Administrator')")
    @DeleteMapping("/{customerId}")
    public ResponseEntity<Void> deleteCustomerById(@PathVariable("customerId") UUID customerId) {
         customerService.deleteCustomer(customerId);
         return ResponseEntity.accepted().build();

    }
}
