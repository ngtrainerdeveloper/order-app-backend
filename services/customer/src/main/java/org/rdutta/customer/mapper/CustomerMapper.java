package org.rdutta.customer.mapper;

import org.rdutta.customer.entity.Customer;
import org.rdutta.customer.record.CustomerRequest;
import org.rdutta.customer.record.CustomerResponse;
import org.springframework.stereotype.Component;

@Component
public class CustomerMapper {
    public Customer toCustomer(CustomerRequest customerRequest) {
        if(customerRequest == null){
            return null;
        }
        return Customer.builder()
                .customerId(customerRequest.customerId())
                .firstName(customerRequest.firstName())
                .lastName(customerRequest.lastName())
                .email(customerRequest.email())
                .phone(customerRequest.phone())
                .address(customerRequest.address())
                .build();
    }

    public CustomerResponse fromCustomer(Customer customer) {
        return new CustomerResponse(
                customer.getCustomerId(),
                customer.getFirstName(),
                customer.getLastName(),
                customer.getEmail(),
                customer.getPhone(),
                customer.getAddress()
        );
    }
}
