package org.rdutta.customer.service;


import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.rdutta.customer.customexception.CustomerNotFoundException;
import org.rdutta.customer.entity.Address;
import org.rdutta.customer.entity.Customer;
import org.rdutta.customer.mapper.CustomerMapper;
import org.rdutta.customer.record.CustomerRequest;
import org.rdutta.customer.record.CustomerResponse;
import org.rdutta.customer.repository.CustomerRepository;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerRepository customerRepository;
    private final CustomerMapper customerMapper;


    public UUID createCustomer(CustomerRequest customerRequest) {

        UUID customerId = customerRequest.customerId();
        UUID localId = customerRequest.address().getLocalId();
        // If customerId is null, generate a new one
        if (customerId == null && localId == null) {
            customerId = UUID.randomUUID();
            localId = UUID.randomUUID();
        }


        // Set the customerId to the Customer entity
        Customer customerEntity = customerMapper.toCustomer(customerRequest);
        customerEntity.setCustomerId(customerId);
        customerEntity.getAddress().setLocalId(localId);

        // Save the customer entity
        var customer = customerRepository.save(customerEntity);

        return customer.getCustomerId();
    }

    public void updateCustomer(CustomerRequest customerRequest) {
        var customer = customerRepository.findById(customerRequest.customerId()).orElseThrow(() -> new CustomerNotFoundException(String.format("Customer with id '%s' not found", customerRequest.customerId())));

        mergerCustomer(customer, customerRequest);
        customerRepository.save(customer);
    }

    private void mergerCustomer(Customer customer, CustomerRequest customerRequest) {
        if (StringUtils.isNotBlank(customerRequest.firstName())) {
            customer.setFirstName(customerRequest.firstName());
        }
        if (StringUtils.isNotBlank(customerRequest.lastName())) {
            customer.setLastName(customerRequest.lastName());
        }
        if (StringUtils.isNotBlank(customerRequest.email())) {
            customer.setEmail(customerRequest.email());
        }
        if (StringUtils.isNotBlank(customerRequest.phone())) {
            customer.setPhone(customerRequest.phone());
        }
        if(customerRequest.address() != null) {

            customer.setAddress(customerRequest.address());
        }
    }

    public List<CustomerResponse> findAllCustomers() {
        return customerRepository.findAll().stream().map(customerMapper::fromCustomer)
                .collect(Collectors.toList());
    }

    public Boolean existsById(UUID customerId) {
        return customerRepository.findById(customerId).isPresent();
    }

    public CustomerResponse findByCustomerId(UUID customerId) {
        return customerRepository.findById(customerId)
                .map(customerMapper::fromCustomer)
                .orElseThrow(() -> new CustomerNotFoundException(String.format("Customer with id '%s' not found", customerId)));
    }

    public void deleteCustomer(UUID customerId) {
        customerRepository.deleteById(customerId);
    }
}
