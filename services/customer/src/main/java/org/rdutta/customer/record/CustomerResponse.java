package org.rdutta.customer.record;


import org.rdutta.customer.entity.Address;

import java.util.UUID;

public record CustomerResponse(
        UUID customerId,
        String firstName,
        String lastName,
        String email,
        String phone,
        Address address
) {
}
