package org.rdutta.customer.entity;

import lombok.*;
import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import java.io.Serializable;
import java.util.UUID;


@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Data
@Table
public class Customer implements Serializable {
    @PrimaryKey
    private UUID customerId;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    @Column("address")
    private Address address;
}
