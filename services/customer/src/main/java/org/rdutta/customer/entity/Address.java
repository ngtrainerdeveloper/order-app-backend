package org.rdutta.customer.entity;

import lombok.*;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;
import org.springframework.data.cassandra.core.mapping.UserDefinedType;
import org.springframework.validation.annotation.Validated;

import java.io.Serializable;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Data
@Validated
@Table
@UserDefinedType("address_udt")
public class Address implements Serializable{
    @PrimaryKey
    private UUID localId;
    private String street;
    private String city;
    private String state;
    private String zip;
    private String country;
}
