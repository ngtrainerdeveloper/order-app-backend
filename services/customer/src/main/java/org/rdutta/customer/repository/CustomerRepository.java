package org.rdutta.customer.repository;

import org.rdutta.customer.entity.Customer;
import org.springframework.data.cassandra.repository.CassandraRepository;

import java.util.UUID;


public interface CustomerRepository extends CassandraRepository<Customer, UUID> {
}
