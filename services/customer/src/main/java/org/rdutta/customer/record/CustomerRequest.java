package org.rdutta.customer.record;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import org.rdutta.customer.entity.Address;

import java.util.UUID;


public record CustomerRequest(
         UUID customerId,
         @NotNull(message = "Customer firstname should not be null.")
         String firstName,
         @NotNull(message = "Customer lastname should not be null.")
         String lastName,
         @NotNull(message = "Email should not be null.")
         @Email(message = "Email is not a valid email address")
         String email,
         String phone,
         Address address
) {

}
