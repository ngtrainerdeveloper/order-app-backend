package org.rdutta.customer.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.datastax.oss.driver.api.core.CqlSession;

@Configuration
public class CassandraConfig {

    @Bean
    public CqlSession cqlSession() {
        return CqlSession.builder()
                .withKeyspace("customer_space")
                .withLocalDatacenter("datacenter1") // Ensure this matches your datacenter
                .build();
    }
}
