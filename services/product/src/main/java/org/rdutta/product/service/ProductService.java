package org.rdutta.product.service;

import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.rdutta.product.customexception.ProductPurchaseException;
import org.rdutta.product.mapper.ProductMapper;
import org.rdutta.product.record.ProductPurchaseRequest;
import org.rdutta.product.record.ProductPurchaseResponse;
import org.rdutta.product.record.ProductRequest;
import org.rdutta.product.record.ProductResponse;
import org.rdutta.product.repository.ProductRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductService {

    private final ProductRepository productRepository;

    private final ProductMapper productMapper;


    public Integer createProduct(
            ProductRequest request
    ) {
        var product = productMapper.toProduct(request);
        return productRepository.save(product).getId();
    }

    public ProductResponse findById(Integer id) {
        return productRepository.findById(id)
                .map(productMapper::toProductResponse)
                .orElseThrow(() -> new EntityNotFoundException("Product not found with ID:: " + id));
    }

    public List<ProductResponse> findAll() {
        return productRepository.findAll()
                .stream()
                .map(productMapper::toProductResponse)
                .collect(Collectors.toList());
    }

    @Transactional(rollbackFor = ProductPurchaseException.class)
    public List<ProductPurchaseResponse> purchaseProducts(List<ProductPurchaseRequest> request) {
        //All the products we want to purchase
        var productId = request.stream().map(ProductPurchaseRequest::productId)
                .toList();

        //All the products already in the Database
        var storedProducts = productRepository.findAllByIdInOrderById(productId);
        if(productId.size() != storedProducts.size()) {
            throw new ProductPurchaseException("One or more products doesn't exists.");
        }

        var storedRequest = request.stream().sorted(Comparator.comparing(ProductPurchaseRequest::productId)).toList();

        var purchasedProducts = new ArrayList<ProductPurchaseResponse>();
        for(int i=0; i<storedRequest.size(); i++) {
            var product = storedProducts.get(i);
            var productRequest = storedRequest.get(i);

            if(product.getProductAvailableQuantity() < productRequest.quantity()){
                throw new ProductPurchaseException("Insufficient quantity for product " + productRequest.productId());
            }

            var newAvailableQuantity = product.getProductAvailableQuantity() - productRequest.quantity();
            product.setProductAvailableQuantity(newAvailableQuantity);
            productRepository.save(product);
            purchasedProducts.add(productMapper.toProductPurchaseResponse(product, productRequest.quantity()));
        }
        return purchasedProducts;
    }

    public ProductResponse findByProductId(Integer productId) {

        return productRepository.findById(productId).map(productMapper::toProductResponse)
                .orElseThrow(()->new EntityNotFoundException("Product not found with ID:: "+productId));
    }

    public List<ProductResponse> findAllProducts() {
        return productRepository.findAll().stream().map(productMapper::toProductResponse).collect(Collectors.toList());
    }
}
