package org.rdutta.product.record;

import jakarta.validation.constraints.NotNull;

public record ProductPurchaseRequest(
        @NotNull(message = "Product id is mandatory")
        Integer productId,
        @NotNull(message = "Available quantity is mandatory")
        Double quantity
) {
}
