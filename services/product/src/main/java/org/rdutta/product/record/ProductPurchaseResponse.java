package org.rdutta.product.record;

import java.math.BigDecimal;

public record ProductPurchaseResponse(
        Integer id,
        String productName,
        String productDescription,
        BigDecimal productPrice,
        Double quantity
) {
}
