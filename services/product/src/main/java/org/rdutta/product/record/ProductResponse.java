package org.rdutta.product.record;

import java.math.BigDecimal;

public record ProductResponse(
         Integer id,
         String productName,
         String productDescription,
         Double productAvailableQuantity,
         BigDecimal productPrice,
         Integer categoryId,
         String categoryName,
         String categoryDescription
) {
}
