package org.rdutta.product.mapper;

import org.rdutta.product.entity.Category;
import org.rdutta.product.entity.Product;
import org.rdutta.product.record.ProductPurchaseResponse;
import org.rdutta.product.record.ProductRequest;
import org.rdutta.product.record.ProductResponse;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
public class ProductMapper {
    public Product toProduct(ProductRequest request) {
        return Product.builder()
                .id(request.id())
                .productName(request.productName())
                .productDescription(request.productDescription())
                .productAvailableQuantity(request.productAvailableQuantity())
                .productPrice(request.productPrice())
                .productCategory(Category.builder()
                        .id(request.categoryId())
                        .build())
                .build();
    }

    public ProductResponse toProductResponse(Product product) {
        return new ProductResponse(
                product.getId(),
                product.getProductName(),
                product.getProductDescription(),
                product.getProductAvailableQuantity(),
                product.getProductPrice(),
                product.getProductCategory().getId(),
                product.getProductCategory().getName(),
                 product.getProductCategory().getDescription()
        );
    }

    public ProductPurchaseResponse toProductPurchaseResponse(Product product, Double quantity) {
        return new ProductPurchaseResponse(
                product.getId(),
                product.getProductName(),
                product.getProductDescription(),
                product.getProductPrice(),
                quantity
        );
    }
}
