package org.rdutta.product.record;

import jakarta.validation.constraints.NotNull;

import java.math.BigDecimal;

public record ProductRequest(
         Integer id,
         @NotNull(message = "Product name is required.")
         String productName,
         @NotNull(message = "Product description is required.")
         String productDescription,
         @NotNull(message = "Product available quantity should be positive.")
         Double productAvailableQuantity,
         @NotNull(message = "Price should be positive.")
         BigDecimal productPrice,
         @NotNull(message = "Product category is required.")
         Integer categoryId
) {
}
