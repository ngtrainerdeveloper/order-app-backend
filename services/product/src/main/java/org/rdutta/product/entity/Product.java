package org.rdutta.product.entity;


import jakarta.persistence.*;
import lombok.*;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Entity
public class Product {
    @Id
    @GeneratedValue
    private Integer id;
    private String productName;
    private String productDescription;
    private Double productAvailableQuantity;
    private BigDecimal productPrice;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category productCategory;
}
