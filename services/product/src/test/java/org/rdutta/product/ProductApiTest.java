package org.rdutta.product;

import com.google.gson.Gson;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.hamcrest.Matchers;
import org.rdutta.product.entity.Category;
import org.rdutta.product.record.ProductRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.math.BigDecimal;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class ProductApiTest {
    private static final Logger log = LoggerFactory.getLogger(ProductApiTest.class);
    private static Integer testCategoryId;

    @BeforeClass
    public static void setUp() {
        RestAssured.baseURI = "http://localhost:8050";
        testCategoryId = 51;

        // Ensure the category exists in the database
        Category category = Category.builder()
                .id(testCategoryId)
                .name("Accessories")
                .description("Computer Accessories")
                .build();

        // Insert the category into the database if needed
        // This can be done through a direct database insert or an API call to create the category
    }

    @AfterClass
    public void teardown() {
        // Cleanup code if necessary
    }

    @Test
    public void createProduct() {
        ProductRequest productRequest = new ProductRequest(
                null, // Assuming the ID is auto-generated
                "External Hard Drive Enclosure 7",
                "External hard drive enclosure with USB-C",
                30.00,
                BigDecimal.valueOf(99.99),
                testCategoryId
        );

        Gson gson = new Gson();
        String jsonBody = gson.toJson(productRequest);

        Integer newProductId = given()
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON)
                .body(jsonBody)
                .when()
                .post("/api/v1/products/create")
                .then()
                .assertThat()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .extract()
                .as(Integer.class);  // Extracting the generated ID

        log.info("Successfully created product with id {}", newProductId);

        // Additional assertions can be made to verify the product creation in the database
    }
}
